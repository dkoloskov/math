//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public ExpressionComponent expression { get { return (ExpressionComponent)GetComponent(GameComponentsLookup.Expression); } }
    public bool hasExpression { get { return HasComponent(GameComponentsLookup.Expression); } }

    public void AddExpression(System.Collections.Generic.List<int> newOperations) {
        var index = GameComponentsLookup.Expression;
        var component = (ExpressionComponent)CreateComponent(index, typeof(ExpressionComponent));
        component.operations = newOperations;
        AddComponent(index, component);
    }

    public void ReplaceExpression(System.Collections.Generic.List<int> newOperations) {
        var index = GameComponentsLookup.Expression;
        var component = (ExpressionComponent)CreateComponent(index, typeof(ExpressionComponent));
        component.operations = newOperations;
        ReplaceComponent(index, component);
    }

    public void RemoveExpression() {
        RemoveComponent(GameComponentsLookup.Expression);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherExpression;

    public static Entitas.IMatcher<GameEntity> Expression {
        get {
            if (_matcherExpression == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.Expression);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherExpression = matcher;
            }

            return _matcherExpression;
        }
    }
}
