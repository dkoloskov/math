public class ConfigSystems : Feature
{
	// CORRECT ORDER OF SYSTEMS ADDING/CREATION
	// 1. Initialize
	// 2. Events (Reactive)
	// 3. Update (Execute)
	// 4. Cleanup

	public ConfigSystems(Contexts contexts) : base("ConfigSystems")
	{
		// +++++++++++++++++++++++++ GENERATE EXPRESSION +++++++++++++++++++++++++
		// Initialize
		Add(new CreateGenerateExpressionConfig(contexts));
		Add(new CreateExpressionViewConfig(contexts));

		// Events (Reactive)

		// Update (Execute)

		// Cleanup
		// ------------------------- GENERATE EXPRESSION -------------------------
	}

	//--------------------------------------------------------------------------
	//							PUBLIC METHODS
	//--------------------------------------------------------------------------
}