﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Config]
[Unique]
public sealed class ExpressionViewConfigComponent : IComponent
{
	public int fontSize;
	
	// Distance in pixels. Operands, operators, etc. 
	public float horizonlalDistanceBetweenMembers;
    public float verticalDistanceBetweenMembers; 
}