﻿using System.Collections.Generic;
using Entitas;
using Entitas.CodeGeneration.Attributes;

[Config]
[Unique]
public sealed class GenerateExpressionConfigComponent : IComponent
{
	public List<OperationType> allowedMathOperations;

	public int minOperationsQuantity;
	public int maxOperationsQuantity;

	public int numberMaxLength; // Examle: "3" - length 1, "15" - length 2, etc.
}