﻿using Entitas;

public sealed class CreateExpressionViewConfig : IInitializeSystem
{
	private readonly Contexts _contexts;

	public CreateExpressionViewConfig(Contexts contexts)
	{
		_contexts = contexts;
	}

	public void Initialize()
	{
		_contexts.config.ReplaceExpressionViewConfig
		(
			100, // fontSize
			25, // horizonlalDistanceBetweenMembers
			50 // verticalDistanceBetweenMembers
		);
	}
}