﻿using System.Collections.Generic;
using Entitas;

public sealed class CreateGenerateExpressionConfig : IInitializeSystem
{
	private readonly Contexts _contexts;

	public CreateGenerateExpressionConfig(Contexts contexts)
	{
		_contexts = contexts;
	}

	public void Initialize()
	{
		_contexts.config.ReplaceGenerateExpressionConfig
		(
			new List<OperationType>()
			{
				OperationType.ADDITION,
				OperationType.SUBSTRACTION,
				OperationType.MULTIPLICATION,
				OperationType.DIVISION
			},
			1, // minOperationsQuantity
			4, // maxOperationsQuantity
			3 // numberMaxLength
		);
	}
}