﻿using UnityEngine;

public interface IViewUtilsService
{
	void DrawView(GameObject viewGO, string text);
	void DrawView(GameObject viewGO, OperationType operationType);
}