﻿using System.Collections.Generic;
using UnityEngine;

// Class for individual view control/tweaks
public class ViewUtilsService : Service, IViewUtilsService
{
	private Dictionary<OperationType, string> operationSymbols;
	private Dictionary<OperationType, Vector3> operationSymbolsViewCorrections;

	public ViewUtilsService(Contexts contexts) : base(contexts)
	{
		initOperationSymbols();
	}

	//--------------------------------------------------------------------------
	//							PUBLIC METHODS
	//--------------------------------------------------------------------------
	// TODO: Move all configs from here to "config" context
	public void DrawView(GameObject viewGO, string text)
	{
		TextMesh textM = viewGO.AddComponent<TextMesh>();
		textM.text = text;
		textM.fontSize = expressionViewConfig.fontSize;
		textM.color = Color.red;
		textM.anchor = TextAnchor.MiddleLeft;

		Transform transform = viewGO.GetComponent<Transform>();
		// TODO: Use NOT default font (with some special parameters, I saved it in video on YouTube) + REMOVE LINE BELOW
		transform.localScale = new Vector2(10, 10);
	}

	public void DrawView(GameObject viewGO, OperationType operationType)
	{
		DrawView(viewGO, operationSymbols[operationType]);

		// Fix position, because symbols: "-" and "*" have different vertical positions.
		Vector3 correction = operationSymbolsViewCorrections[operationType];
		Vector3 currentPos = viewGO.transform.position;
		viewGO.transform.position = new Vector3(
			currentPos.x + correction.x,
			currentPos.y + correction.y,
			currentPos.z + correction.z
		);
	}

	// This method NOT used, but, shouldn't be removed! Good example of how to get real TextMesh width (or height)
	// if it was transformed: rotated, etc.
	public float GetTextMeshWidth(Transform view)
	{
		TextMesh textMesh = view.GetComponent<TextMesh>();
		int textMeshWidth = 0;
		foreach (char symbol in textMesh.text)
		{
			CharacterInfo info;
			if (textMesh.font.GetCharacterInfo(symbol, out info, textMesh.fontSize, textMesh.fontStyle))
			{
				textMeshWidth += info.advance;
			}
		}

		return textMeshWidth;
	}

	//--------------------------------------------------------------------------
	//					  PRIVATE\PROTECTED METHODS
	//--------------------------------------------------------------------------
	private void initOperationSymbols()
	{
		operationSymbols = new Dictionary<OperationType, string>();
		operationSymbols.Add(OperationType.ADDITION, "+");
		operationSymbols.Add(OperationType.SUBSTRACTION, "-");
		operationSymbols.Add(OperationType.MULTIPLICATION, "*");

		operationSymbolsViewCorrections = new Dictionary<OperationType, Vector3>();
		operationSymbolsViewCorrections.Add(OperationType.ADDITION, Vector3.zero);
		operationSymbolsViewCorrections.Add(OperationType.SUBSTRACTION, new Vector3(0, 9, 0));
		operationSymbolsViewCorrections.Add(OperationType.MULTIPLICATION, new Vector3(0, -23, 0));
	}

	//--------------------------------------------------------------------------
	//							GETTERS/SETTERS (PROPERTIES)
	//--------------------------------------------------------------------------
	private ExpressionViewConfigComponent expressionViewConfig
	{
		get { return _contexts.config.expressionViewConfig; }
	}

	// TODO: REFACTOR EVERYTHING BELOW ===================================
	public static GameObject NewTaskMemberView(string value)
	{
		GameObject view = newTextMesh(value);
		return view;
	}

	public static GameObject NewTaskOperatorView(string @operator)
	{
		GameObject view = newTextMesh(@operator);
		return view;
	}

	public static GameObject NewDivisionOperatorView(float divisionLineLength)
	{
		string divisionText = "";
		for (int i = 0; i < divisionLineLength; i++)
		{
			divisionText += "_";
		}

		GameObject view = newTextMesh(divisionText);
		return view;
	}

	private static GameObject newTextMesh(string text)
	{
		GameObject view = new GameObject(text);
		view.AddComponent<TextMesh>();

		TextMesh textM = view.GetComponent<TextMesh>();
		textM.text = text;
		textM.fontSize = 100;
		textM.color = Color.red;

		Transform transform = view.GetComponent<Transform>();
		transform.localScale = new Vector2(10, 10);

		return view;
	}
}