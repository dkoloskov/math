﻿using System.Collections.Generic;
using Entitas;

public interface IViewService
{
	// Create a view from a pre made asset (e.g. a prefab)
	void LoadView(IEntity entity, string assetName = null);

	// Set correct positions of math objects (numbers, operators, etc) relatively to each other.
	void UpdatePositions(List<int> expressionMembersDisplayOrder);
}