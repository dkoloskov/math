using System;
using System.Collections.Generic;
using System.Linq;
using Entitas;
using Entitas.Unity;
using UnityEngine;

// Class for global view's control
public class ViewService : Service, IViewService
{
	private const string EMPTY_VIEW_NAME = "EmptyView";

	private GameObject _taskBlock;
	private GameObject _solvingBlock;

	public ViewService(Contexts contexts) : base(contexts)
	{
		setLayoutBlocks();
	}

	//--------------------------------------------------------------------------
	//							PUBLIC METHODS
	//--------------------------------------------------------------------------
	public void LoadView(IEntity e, string assetName = null)
	{
		GameEntity entity = (GameEntity) e;

		if (assetName == null)
			assetName = EMPTY_VIEW_NAME;

		GameObject viewParanet = (entity.layoutBlock.value == LayoutBlock.TASK_BLOCK) ? _taskBlock : _solvingBlock;
		// I write view here, to make it clear, that this GameObject main purpuse is VIEW
		var viewGO = GameObject.Instantiate(Resources.Load<GameObject>(assetName), viewParanet.transform);
		if (viewGO == null)
			throw new NullReferenceException(string.Format("Prefabs/{0} not found!", assetName));
		viewGO.name = prepareViewGOName(entity);
		viewGO.Link(e);
		
		// TODO: Create cache/dictionary with view GO, to retrieve it FAST by ID.

		// Register listeners
		var eventListeners = viewGO.GetComponents<IEventListener>();
		foreach (var listener in eventListeners)
			listener.RegisterListeners(e);
	}

	// TODO: When solving will be implemented, each solution step must look as diff between steps
	// So, update positions will be implemented only to changed expressions. (maybe refactoring of this method will be required)
	public void UpdatePositions(List<int> expressionMembersDisplayOrder)
	{
		// Calculate expression block size
		GameObject viewGO;
		float childrenWidth = 0;
		float childrenHeight = 0;
		MeshRenderer meshRenderer;
		foreach (int id in expressionMembersDisplayOrder)
		{
			//childrenWidth += child.GetComponent<RectTransform>().rect.width;
			
			// TODO: get object from dictionary/cache instead of GameObject.Find(...)
			viewGO = GameObject.Find("id:"+id);
			meshRenderer = viewGO.GetComponent<MeshRenderer>();
			childrenWidth += meshRenderer.bounds.size.x;
			childrenHeight += meshRenderer.bounds.size.y;

			Debug.Log("number width: " + meshRenderer.bounds.size.x + ", height: " + meshRenderer.bounds.size.y);
		}

		Debug.Log("childrenWidth: " + childrenWidth + ", childrenHeight: " + childrenHeight);

		// size + distance between memebers
		childrenWidth += expressionViewConfig.horizonlalDistanceBetweenMembers * (expressionMembersDisplayOrder.Count - 1);
		// TODO: add vertical distance

		// Update positions
		float newViewX = -childrenWidth / 2;
		foreach (int id in expressionMembersDisplayOrder)
		{
			// TODO: get object from dictionary/cache instead of GameObject.Find(...)
			viewGO = GameObject.Find("id:"+id);
			viewGO.transform.position = new Vector3(viewGO.transform.position.x + newViewX, viewGO.transform.position.y);
			meshRenderer = viewGO.GetComponent<MeshRenderer>();

			newViewX += meshRenderer.bounds.size.x + expressionViewConfig.horizonlalDistanceBetweenMembers;
		}

		// TODO: add line breaks
	}

	//--------------------------------------------------------------------------
	//					  PRIVATE\PROTECTED METHODS
	//--------------------------------------------------------------------------
	private void setLayoutBlocks()
	{
		_taskBlock = GameObject.Find("TaskBlock");
		_solvingBlock = GameObject.Find("SolvingBlock");
	}

	// This method mostly for comfortable debug/development process.
	private string prepareViewGOName(GameEntity entity)
	{
		/*string name = "(id:" + entity.id.value.ToString() + ")";
		if (entity.number != null)
		{
			name = "number:" + entity.number.value.ToString() + "_" + name;
		}*/
		string name = "id:" + entity.id.value.ToString();
		return name;
	}

	//--------------------------------------------------------------------------
	//								HANDLERS
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	//							GETTERS/SETTERS (PROPERTIES)
	//--------------------------------------------------------------------------
	private ExpressionViewConfigComponent expressionViewConfig
	{
		get { return _contexts.config.expressionViewConfig; }
	}

	//--------------------------------------------------------------------------
	//							   DESTROY
	//--------------------------------------------------------------------------
}