using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Unique]
public sealed class ViewServiceComponent : IComponent
{
	public IViewService instance;
}