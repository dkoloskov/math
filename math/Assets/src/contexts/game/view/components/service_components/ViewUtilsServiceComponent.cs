using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Unique]
public sealed class ViewUtilsServiceComponent : IComponent
{
	public IViewUtilsService instance;
}