using System.Collections.Generic;
using Entitas;

public class ViewSystem : ReactiveSystem<GameEntity>
{
	private readonly Contexts _contexts;

	public ViewSystem(Contexts contexts) : base(contexts.game)
	{
		_contexts = contexts;
	}

	//--------------------------------------------------------------------------
	//							PUBLIC METHODS
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	//					  PRIVATE\PROTECTED METHODS
	//--------------------------------------------------------------------------
	protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
	{
		return context.CreateCollector(GameMatcher.View.Added());
	}

	protected override bool Filter(GameEntity entity)
	{
		return entity.hasView; // && !entity.isAssetLoaded;
	}

	//--------------------------------------------------------------------------
	//								HANDLERS
	//--------------------------------------------------------------------------	
	protected override void Execute(List<GameEntity> entities)
	{
		foreach (var entity in entities)
		{
			// call the view service to make a new view
			viewService.LoadView(entity, entity.view.assetName);
			if (entity.view.assetName == null)
			{
				entity.isDrawView = true;
			}

			//entity.isAssetLoaded = true;
		}
	}


	//--------------------------------------------------------------------------
	//							GETTERS/SETTERS (PROPERTIES)
	//--------------------------------------------------------------------------
	private IViewService viewService
	{
		get { return _contexts.game.viewService.instance; }
	}
}