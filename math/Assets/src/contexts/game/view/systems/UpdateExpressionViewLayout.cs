﻿using System.Collections.Generic;
using Entitas;

// System for update of text and other view elements positions (relatively to screen and each other)
public class UpdateExpressionViewLayout : ReactiveSystem<GameEntity>
{
	private readonly Contexts _contexts;

	public UpdateExpressionViewLayout(Contexts contexts) : base(contexts.game)
	{
		_contexts = contexts;
	}

	//--------------------------------------------------------------------------
	//							PUBLIC METHODS
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	//					  PRIVATE\PROTECTED METHODS
	//--------------------------------------------------------------------------
	protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
	{
		return context.CreateCollector(GameMatcher.DrawView.Added());
	}

	protected override bool Filter(GameEntity entity)
	{
		return entity.isDrawView && entity.hasOperation;
	}

	// Prepare display order from left to right
	private List<int> prepareExpressionMembersDisplayOrder(List<GameEntity> entities)
	{
		List<int> displayOrder = new List<int>();
		int operandID;
		foreach (GameEntity entity in entities)
		{
			// Add first operand
			operandID = entity.operation.operands[0];
			if (!displayOrder.Contains(operandID))
				displayOrder.Add(operandID);
			
			// Add operator
			displayOrder.Add(entity.id.value);

			operandID = entity.operation.operands[1];
			if (!displayOrder.Contains(operandID))
				displayOrder.Add(operandID);
		}
		
		return displayOrder;
	}
	
	//--------------------------------------------------------------------------
	//								HANDLERS
	//--------------------------------------------------------------------------	
	protected override void Execute(List<GameEntity> entities)
	{
		List<int> expressionMembersDisplayOrder = prepareExpressionMembersDisplayOrder(entities);
		viewService.UpdatePositions(expressionMembersDisplayOrder);
	}

	//--------------------------------------------------------------------------
	//							GETTERS/SETTERS (PROPERTIES)
	//--------------------------------------------------------------------------
	private IViewService viewService
	{
		get { return _contexts.game.viewService.instance; }
	}
}