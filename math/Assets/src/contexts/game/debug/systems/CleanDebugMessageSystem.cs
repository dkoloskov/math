using Entitas;

public class CleanDebugMessageSystem : ICleanupSystem
{
	private readonly GameContext _context;
	private readonly IGroup<GameEntity> _debugMessages;

	public CleanDebugMessageSystem(Contexts contexts)
	{
		_context = contexts.game;
		_debugMessages = _context.GetGroup(GameMatcher.DebugMessage);
	}

	//--------------------------------------------------------------------------
	//							PUBLIC METHODS
	//--------------------------------------------------------------------------
	public void Cleanup()
	{
		// group.GetEntities() always gives us an up to date list
		foreach (var e in _debugMessages.GetEntities())
		{
			e.Destroy();
		}
	}

	//--------------------------------------------------------------------------
	//					  PRIVATE\PROTECTED METHODS
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	//								HANDLERS
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	//							GETTERS/SETTERS (PROPERTIES)
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	//							   DESTROY
	//--------------------------------------------------------------------------
}