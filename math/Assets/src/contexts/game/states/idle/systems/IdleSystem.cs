using System.Collections.Generic;
using Entitas;
using UnityEngine;

public class IdleSystem : ReactiveSystem<GameEntity>
{
	private readonly Contexts _contexts;

	public IdleSystem(Contexts contexts) : base(contexts.game)
	{
		_contexts = contexts;
	}

	//--------------------------------------------------------------------------
	//							PUBLIC METHODS
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	//					  PRIVATE\PROTECTED METHODS
	//--------------------------------------------------------------------------
	protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
	{
		return context.CreateCollector(GameMatcher.GameState.Added());
	}

	protected override bool Filter(GameEntity entity)
	{
		return entity.gameState.state == GameState.IDLE; // && !entity.isAssetLoaded;
	}

	//--------------------------------------------------------------------------
	//								HANDLERS
	//--------------------------------------------------------------------------	
	protected override void Execute(List<GameEntity> entities)
	{
		//Debug.Log("Game state:("+ _contexts.game.gameState +") Execute()");
		
		_contexts.game.ReplaceGameState(GameState.GENERATE_EXPRESSION);
	}
	
	
	//--------------------------------------------------------------------------
	//							GETTERS/SETTERS (PROPERTIES)
	//--------------------------------------------------------------------------
	/*private IViewService viewService
	{
		get { return _contexts.game.viewService.instance; }
	}*/

}