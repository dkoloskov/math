﻿using Entitas;

public sealed class InitDefaultStateSystem : IInitializeSystem
{
	private readonly Contexts _contexts;

	public InitDefaultStateSystem(Contexts contexts)
	{
		_contexts = contexts;
	}

	public void Initialize()
	{
		//Debug.Log("InitDefaultStateSystem.Initialize()");

		_contexts.game.ReplaceGameState(GameState.IDLE);
	}
}