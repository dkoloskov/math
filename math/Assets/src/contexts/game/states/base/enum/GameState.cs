public enum GameState
{
	// TODO: Prepare loading configs
	// LoadConfigs
	
	IDLE, // Game idle state
	// Trigger by: default
	// Exits to: GenerateExpression
	
	GENERATE_EXPRESSION,
	// Trigger by: GenerateExpressionAction
	// Exits to: SolveExpression 
	
	SOLVE_EXPRESSION,
	// Trigger by: SolveExpressionAction
	// Extits to: TBD
}