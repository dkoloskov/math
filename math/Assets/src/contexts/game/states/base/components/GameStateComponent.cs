using Entitas;
using Entitas.CodeGeneration.Attributes;

/**
 * This compoment mostly for visibility of game state during debug, will not be used in game logic.
 * Actual game state is - game components
 */
[Game, Unique]
public sealed class GameStateComponent : IComponent
{
	public GameState state;
}