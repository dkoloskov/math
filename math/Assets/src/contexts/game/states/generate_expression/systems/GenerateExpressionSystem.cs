using System.Collections.Generic;
using Entitas;

public class GenerateExpressionSystem : ReactiveSystem<GameEntity>
{
	private readonly Contexts _contexts;

	public GenerateExpressionSystem(Contexts contexts) : base(contexts.game)
	{
		_contexts = contexts;
	}

	//--------------------------------------------------------------------------
	//							PUBLIC METHODS
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	//					  PRIVATE\PROTECTED METHODS
	//--------------------------------------------------------------------------
	protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
	{
		return context.CreateCollector(GameMatcher.GameState.Added());
	}

	protected override bool Filter(GameEntity entity)
	{
		return entity.gameState.state == GameState.GENERATE_EXPRESSION;
	}

	// TODO: Whene generateNEW() will be finished, remove from methods name word NEW()
	private void generateNEW()
	{
		// TODO: Add in algorithm:
		// 1. Expression entity creation with adding created operations.

		// GENERATE ALGORITHM. STEP 1: Create first operand
		GameEntity operandEntity = mathObjectsService.CreateNumberOperand(randomService.GenerateRandomNumber());
		operandEntity.AddLayoutBlock(LayoutBlock.TASK_BLOCK);

		// GENERATE ALGORITHM. STEP 2: In Loop
		int operationsQuantity = randomService.GenerateExpressionOperationsQuantity();
		OperationType operationType;
		GameEntity operationEntity;
		GameEntity secondOperandEntity;
		for (int i = 0; i < operationsQuantity; i++)
		{
			// GENERATE ALGORITHM. STEP 2.1: Create operation (not operand, because square root doesn't require additional value most of the time)
			operationType = randomService.GenerateRandomOperationType();
			operationEntity = mathObjectsService.CreateOperation(operationType);


			// TODO: CONTINUE FROM HERE <<<  <<<  <<<  <<<  <<<  <<<  <<<  <<<  <<<  <<<  
			// 1. generate new operand
			// 2. Find existend/second operand, which can be used in OPERATION with new operand.
			// FIND ALGORITHM
			// 2.1. Get all existent operands
			// 2.2. Get all existent operations. Expression of operation between two operands, can be used as OPERAND! Example: "2+3" - expression can be used as OPERAND.
			// 2.3. Get all expressions groups combinations, for instance, expression: 5 + 3 - 2 + 1:
				// 5 + 3 - 2
				// 5 + 3 - 2 + 1
				// 3 - 2 + 1
			// To find all existent operands and operations, groups can be used. Examples below:
			// gameContext.GetGroup(GameMatcher.Position).GetEntities();
			//_gameBoardElements = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.GameBoardElement, GameMatcher.Position));
			// gameContext.GetEntities(GameMatcher.Movable)


			// GENERATE ALGORITHM. STEP 2.2: Create new operand (if required)
		}


		// GENERATE ALGORITHM. STEP 2.3: Prepare all available members/expressions which can be used with created operation and new operand.
		// GENERATE ALGORITHM. STEP 2.4: Pick random member/expression to use with new operation & operand.

		// TODO: After above algorithm will be ready add in algorithm:
		// Brackets creation: after example generation or duirng, or both.
	}

	// TODO: Whene generateNEW() will be finished, remove this.generate() method.
	private void generate()
	{
		int expressionMembersQuantity = randomService.GenerateExpressionOperationsQuantity();
		GameEntity operandEntity;
		GameEntity previousOperandEntity = null;
		GameEntity operationEntity;
		OperationType operationType;
		for (int i = 0; i < expressionMembersQuantity; i++)
		{
			// Generate number
			operandEntity = mathObjectsService.CreateNumberOperand(randomService.GenerateRandomNumber());
			operandEntity.AddLayoutBlock(LayoutBlock.TASK_BLOCK);

			// Generate operation
			if (previousOperandEntity != null)
			{
				operationType = randomService.GenerateRandomOperationType();
				operationEntity = mathObjectsService.CreateOperation(operationType);
				operationEntity.AddLayoutBlock(LayoutBlock.TASK_BLOCK);

				operationEntity.operation.operands.Add(previousOperandEntity.id.value);
				operationEntity.operation.operands.Add(operandEntity.id.value);
			}

			previousOperandEntity = operandEntity;
		}
	}

	//--------------------------------------------------------------------------
	//								HANDLERS
	//--------------------------------------------------------------------------	
	protected override void Execute(List<GameEntity> entities)
	{
		//Debug.Log("Game state:(" + _contexts.game.gameState + ") Execute()");

		generate();

		// TODO: Remove this
		//_context.CreateEntity().AddDebugMessage("Hello qwerty");

		_contexts.game.ReplaceGameState(GameState.SOLVE_EXPRESSION);
	}

	//--------------------------------------------------------------------------
	//							GETTERS/SETTERS (PROPERTIES)
	//--------------------------------------------------------------------------
	private IMathObjectsService mathObjectsService
	{
		get { return _contexts.game.mathObjectsService.instance; }
	}

	private IRandomService randomService
	{
		get { return _contexts.game.randomService.instance; }
	}
}