using System.Collections.Generic;
using Entitas;
using UnityEngine;

public class SolveExpressionSystem : ReactiveSystem<GameEntity>
{
	private readonly Contexts _contexts;

	public SolveExpressionSystem(Contexts contexts) : base(contexts.game)
	{
		_contexts = contexts;
	}

	//--------------------------------------------------------------------------
	//							PUBLIC METHODS
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	//					  PRIVATE\PROTECTED METHODS
	//--------------------------------------------------------------------------
	protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
	{
		return context.CreateCollector(GameMatcher.GameState.Added());
	}

	protected override bool Filter(GameEntity entity)
	{
		return entity.gameState.state == GameState.SOLVE_EXPRESSION; // && !entity.isAssetLoaded;
	}

	//--------------------------------------------------------------------------
	//								HANDLERS
	//--------------------------------------------------------------------------	
	protected override void Execute(List<GameEntity> entities)
	{
		//Debug.Log("Game state:("+ _contexts.game.gameState +") Execute()");
	}
	
	
	//--------------------------------------------------------------------------
	//							GETTERS/SETTERS (PROPERTIES)
	//--------------------------------------------------------------------------
	/*private IViewService viewService
	{
		get { return _contexts.game.viewService.instance; }
	}*/

}