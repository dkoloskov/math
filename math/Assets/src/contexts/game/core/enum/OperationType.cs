﻿public enum OperationType
{
	ADDITION, // +
	SUBSTRACTION, // -
	MULTIPLICATION, // *
	DIVISION // /
	
	// TODO: TBD other operations: exponent, square, etc
}