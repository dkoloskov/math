using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Unique]
public sealed class IdServiceComponent : IComponent
{
	public IIdService instance;
}