using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Unique]
public sealed class RandomServiceComponent : IComponent
{
	public IRandomService instance;
}