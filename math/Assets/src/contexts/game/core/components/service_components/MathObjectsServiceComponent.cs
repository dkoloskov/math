using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Unique]
public sealed class MathObjectsServiceComponent : IComponent
{
	public IMathObjectsService instance;
}