﻿using System.Collections.Generic;
using Entitas;

[Game]
public sealed class OperationComponent : IComponent
{
	public OperationType type;
	public List<int> operands;
}