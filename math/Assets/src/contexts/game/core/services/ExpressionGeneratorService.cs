﻿public class ExpressionGeneratorService : Service
{
	// TODO: 90% that this class is unnecessary and will be removed, because right now generation of expressions
	// is in separete state. TBD: remove/or move here all requred logic.
	public ExpressionGeneratorService(Contexts contexts) : base(contexts)
	{
	}

	//--------------------------------------------------------------------------
	//							PUBLIC METHODS
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	//					  PRIVATE\PROTECTED METHODS
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	//								HANDLERS
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	//							GETTERS/SETTERS (PROPERTIES)
	//--------------------------------------------------------------------------
	private IIdService idService
	{
		get { return _contexts.game.idService.instance; }
	}

	//--------------------------------------------------------------------------
	//							   DESTROY
	//--------------------------------------------------------------------------
}