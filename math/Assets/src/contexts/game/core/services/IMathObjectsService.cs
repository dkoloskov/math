﻿public interface IMathObjectsService
{
	//--------------------------------------------------------------------------
	//							PUBLIC METHODS
	//--------------------------------------------------------------------------
	GameEntity CreateNumberOperand(float number);
	GameEntity CreateOperation(OperationType operationType);
}