﻿using System.Collections.Generic;

public class MathObjectsService : Service, IMathObjectsService
{
	public MathObjectsService(Contexts contexts) : base(contexts)
	{
	}

	//--------------------------------------------------------------------------
	//							PUBLIC METHODS
	//--------------------------------------------------------------------------
	public GameEntity CreateNumberOperand(float number)
	{
		GameEntity entity = _contexts.game.CreateEntity();
		entity.AddId(idService.GetNext());
		entity.AddNumber(number);

		// VIEW
		entity.AddView(null);

		return entity;
	}

	public GameEntity CreateOperation(OperationType operationType)
	{
		GameEntity entity = _contexts.game.CreateEntity();
		entity.AddId(idService.GetNext());
		entity.AddOperation(operationType, new List<int>());

		// VIEW
		entity.AddView(null);

		return entity;
	}

	//--------------------------------------------------------------------------
	//					  PRIVATE\PROTECTED METHODS
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	//								HANDLERS
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	//							GETTERS/SETTERS (PROPERTIES)
	//--------------------------------------------------------------------------
	private IIdService idService
	{
		get { return _contexts.game.idService.instance; }
	}

	//--------------------------------------------------------------------------
	//							   DESTROY
	//--------------------------------------------------------------------------
}