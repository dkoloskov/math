﻿public interface IRandomService
{
	int GenerateExpressionOperationsQuantity();
	int GenerateRandomNumber();
	OperationType GenerateRandomOperationType();
}