﻿using UnityEngine;

public class RandomService : Service, IRandomService
{
	public RandomService(Contexts contexts) : base(contexts)
	{
	}

	//--------------------------------------------------------------------------
	//							PUBLIC METHODS
	//--------------------------------------------------------------------------
	public int GenerateExpressionOperationsQuantity()
	{
		//int quantity = Random.Range(generateExpressionConfig.minOperationsQuantity, generateExpressionConfig.maxOperationsQuantity);
		int quantity = 3;
		return quantity;
	}

	public int GenerateRandomNumber()
	{
		//int numberLength = Random.Range(1, generateExpressionConfig.numberMaxLength + 1);
		int numberLength = 3;
		int maxNumber = (int) Mathf.Pow(10, numberLength);
		// for numberLength:1 -> maxNumber: 10 -> return value 1..9
		// for numberLength:2 -> maxNumber: 100 -> return value 1..99
		// for numberLength:3 -> maxNumber: 1000 -> return value 1..999
		// etc
		// Random.RandomRange() second parameter for NOT INCLUDED (when passed parameter is "int" type)	

		int randomNumber = Random.Range(1, maxNumber);
		return randomNumber;
	}

	public OperationType GenerateRandomOperationType()
	{
		int operationIndex = Random.Range(0, generateExpressionConfig.allowedMathOperations.Count);
		OperationType type = (OperationType) operationIndex;

		return type;
	}

	//--------------------------------------------------------------------------
	//					  PRIVATE\PROTECTED METHODS
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	//								HANDLERS
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	//							GETTERS/SETTERS (PROPERTIES)
	//--------------------------------------------------------------------------
	private GenerateExpressionConfigComponent generateExpressionConfig
	{
		get { return _contexts.config.generateExpressionConfig; }
	}

	//--------------------------------------------------------------------------
	//							   DESTROY
	//--------------------------------------------------------------------------
}