public class GameSystems : Feature
{
	private const string NAME = "GameSystems";

	// CORRECT ORDER OF SYSTEMS ADDING/CREATION
	// 1. Initialize
	// 2. Events (Reactive)
	// 3. Update (Execute)
	// 4. Cleanup

	public GameSystems(Contexts contexts) : base(NAME)
	{
		// +++++++++++++++++++++++++ COMMON +++++++++++++++++++++++++
		// Initialize
		Add(new InitializeGameServicesSystem(contexts));

		// Events (Reactive)
		//Add(new InputEventSystems(contexts));
		//Add(new GameEventSystems(contexts));
		//Add(new GameStateEventSystems(contexts));
		/////////// VIEW ///////////
		// VIEW STEP 1. Create & add to scree view
		Add(new ViewSystem(contexts));
		// VIEW STEP 2. Process events on MonoBehavior/View Game Objects: Draw view, change position, etc (changes of view on the scree)
		Add(new GameEventSystems(contexts)); // System auto-generated (but this line, must be manually added here)
		// VIEW STEP 3. Update layout (update positions of view elements)
		Add(new UpdateExpressionViewLayout(contexts));
		/////////// VIEW ///////////
		Add(new DebugMessageSystem(contexts));

		// Update (Execute)
		//Add(new GameBoardSystems(contexts));
		//Add(new ScoreSystem(contexts));

		// Cleanup
		Add(new CleanDebugMessageSystem(contexts));
		// ------------------------- COMMON -------------------------

		// TODO: Move all states systems to GameStatesSystem
		// +++++++++++++++++++++++++ STATES +++++++++++++++++++++++++
		// ################## COMMON
		// Initialize
		Add(new InitDefaultStateSystem(contexts));

		// ################## IDLE
		// Events (Reactive)
		Add(new IdleSystem(contexts));

		// ################## GENERATE EXPRESSION
		// Events (Reactive)
		Add(new GenerateExpressionSystem(contexts));

		// ################## SOLVE EXPRESSION
		// Events (Reactive)
		Add(new SolveExpressionSystem(contexts));
		// ------------------------- STATES -------------------------

		// TODO: separate Systems not by purpose, but, by tasks/features AND in 
	}

	//--------------------------------------------------------------------------
	//							PUBLIC METHODS
	//--------------------------------------------------------------------------
}