using Entitas;

public class InitializeGameServicesSystem : IInitializeSystem
{
	private readonly Contexts _contexts;

	public InitializeGameServicesSystem(Contexts contexts)
	{
		_contexts = contexts;
	}

	//--------------------------------------------------------------------------
	//							PUBLIC METHODS
	//--------------------------------------------------------------------------
	public void Initialize()
	{
		// core
		_contexts.game.ReplaceIdService(new IdService());
		_contexts.game.ReplaceRandomService(new RandomService(_contexts));
		_contexts.game.ReplaceMathObjectsService(new MathObjectsService(_contexts));

		// view
		_contexts.game.ReplaceViewService(new ViewService(_contexts));
		_contexts.game.ReplaceViewUtilsService(new ViewUtilsService(_contexts));
	}

	//--------------------------------------------------------------------------
	//					  PRIVATE\PROTECTED METHODS
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	//							GETTERS/SETTERS (PROPERTIES)
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	//							   DESTROY
	//--------------------------------------------------------------------------
}