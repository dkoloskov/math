public class RootSystems : Feature
{
	public RootSystems(Contexts contexts)
	{
		// Config
		Add(new ConfigSystems(contexts));

		// Input

		// Game
		Add(new GameSystems(contexts)); 
	}
}