using Entitas;
using UnityEngine;

public class DrawViewListener : MonoBehaviour, IEventListener, IDrawViewListener
{
	//--------------------------------------------------------------------------
	//							PUBLIC METHODS
	//--------------------------------------------------------------------------
	public void RegisterListeners(IEntity e)
	{
		//Debug.Log("DrawViewListener.RegisterListeners()");

		GameEntity entity = (GameEntity) e;
		entity.AddDrawViewListener(this);
	}

	//--------------------------------------------------------------------------
	//					  PRIVATE\PROTECTED METHODS
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	//								HANDLERS
	//--------------------------------------------------------------------------
	public void OnDrawView(GameEntity entity)
	{
		//Debug.Log("DrawViewListener.OnDrawView()");

		// TODO: Add draw also for variables
		if (entity.hasNumber)
			viewUtilsService.DrawView(gameObject, entity.number.value.ToString());
		else if (entity.hasOperation)
			viewUtilsService.DrawView(gameObject, entity.operation.type);
	}

	//--------------------------------------------------------------------------
	//							GETTERS/SETTERS (PROPERTIES)
	//--------------------------------------------------------------------------
	private IViewUtilsService viewUtilsService
	{
		get { return Contexts.sharedInstance.game.viewUtilsService.instance; }
	}
}