using Entitas;
using UnityEngine;

public class GameController : MonoBehaviour
{
	private Systems _systems;

	//--------------------------------------------------------------------------
	//					  PRIVATE\PROTECTED METHODS
	//--------------------------------------------------------------------------
	private void Awake()
	{
		_systems = new RootSystems(Contexts.sharedInstance);
	}

	void Start()
	{
		// Required to generate unique random combinations each game session
		UnityEngine.Random.InitState(System.DateTime.Now.Millisecond);
		
		// call Initialize() on all of the IInitializeSystems
		_systems.Initialize();
	}

	//--------------------------------------------------------------------------
	//								HANDLERS
	//--------------------------------------------------------------------------
	void Update()
	{
		// call Execute() on all the IExecuteSystems and 
		// ReactiveSystems that were triggered last frame
		_systems.Execute();

		// call cleanup() on all the ICleanupSystems
		_systems.Cleanup();
	}

	//--------------------------------------------------------------------------
	//							   DESTROY
	//--------------------------------------------------------------------------
	void OnDestroy()
	{
		_systems.TearDown();
	}
}